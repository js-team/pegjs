# Installation
> `npm install --save @types/pegjs`

# Summary
This package contains type definitions for PEG.js (http://pegjs.org/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pegjs.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 20:19:31 GMT
 * Dependencies: none
 * Global values: `PEG`

# Credits
These definitions were written by [vvakame](https://github.com/vvakame), [Tobias Kahlert](https://github.com/SrTobi), and [C.J. Bell](https://github.com/siegebell).
